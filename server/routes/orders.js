var express = require('express');
var router = express.Router();
const authController = require('../controllers').auth;
const orderController = require('../controllers').order;

// GET
// List all orders
router.get('/all', orderController.all);
// Get waiting time estimation
router.get('/waitingtime', orderController.waitingTime);

// POST
// Create new order from customer
router.post('/create', authController.isCustomer, orderController.create);

//POST
// Get dish to be assigned to requesting chef
router.put('/assignchef', authController.isChef, orderController.assignChef);

module.exports = router;