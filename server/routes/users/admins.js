var express = require('express');
var router = express.Router();
const authController = require('../../controllers').auth;
const adminController = require('../../controllers').admin;

// GET
// List all admins
router.get('/all', authController.isAdmin, adminController.all);

// POST
// Create a new admin
router.post('/create', authController.isAdmin, adminController.create);

// DELETE
// Delete an admin
router.delete('/delete', authController.isAdmin, adminController.delete);

module.exports = router;