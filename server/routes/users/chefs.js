var express = require('express');
var router = express.Router();
const authController = require('../../controllers').auth;
const chefController = require('../../controllers').chef;

// GET
// List all chefs
router.get('/all', authController.isAdmin, chefController.all);

// POST
// Create a new chef
router.post('/create', authController.isAdmin, chefController.create);

// DELETE
// Delete a chef
router.delete('/delete', authController.isAdmin, chefController.delete);

module.exports = router;