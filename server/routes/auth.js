var express = require('express');
var router = express.Router();
const authController = require('../controllers').auth;

// POST
// Signup a new customer
router.post('/signup', authController.signup);
// Login API for all user
router.post('/login', authController.login);

module.exports = router;
