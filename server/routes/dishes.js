var express = require('express');
var router = express.Router();
const authController = require('../controllers').auth;
const dishController = require('../controllers').dish;
const multer = require('multer');

// Storage config for dishes' images
var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/images')
    },
    filename: (req, file, cb) => {
        cb(null, (req.body.id ? req.body.id : req.id) + '-' + file.originalname);
    }
});
var upload = multer({ storage: storage });

// GET 
// List all dishes
router.get('/all', dishController.all);

// POST
// Create a new dish
router.post('/create', authController.isAdmin, dishController.create, upload.any('image'), dishController.update);

// PUT
// Update a dish
router.put('/update', authController.isAdmin, upload.any('image'), dishController.update);

// DELETE
// Delete a dish
router.delete('/delete', authController.isAdmin, dishController.delete);

module.exports = router;