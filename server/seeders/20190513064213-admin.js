const crypto = require('crypto');
const userConstants = require('../constants').user;

'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */

    // Create admin
    // creating a unique salt for a particular user 
    let salt = crypto.randomBytes(16).toString('hex');
    // hashing user's salt and password with 1000 iterations, 64 length and sha512 digest 
    let hash = crypto.pbkdf2Sync("admin123", salt, 1000, 64, `sha512`).toString(`hex`);
    return queryInterface.bulkInsert('Users', [{
      email: 'admin',
      password: hash,
      salt: salt,
      role: userConstants.role.admin,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
