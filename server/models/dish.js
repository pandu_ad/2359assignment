'use strict';
module.exports = (sequelize, DataTypes) => {
  const Dish = sequelize.define('Dish', {
    description: DataTypes.STRING,
    image: DataTypes.STRING,
    price: DataTypes.INTEGER,
    duration: DataTypes.INTEGER
  }, {});
  Dish.associate = function (models) {
    // associations can be defined here
    Dish.hasMany(models.OrderItem, {
      foreignKey: 'dishId',
      as: 'orderItems'
    });
  };
  return Dish;
};