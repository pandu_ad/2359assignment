'use strict';
module.exports = (sequelize, DataTypes) => {
  const OrderItem = sequelize.define('OrderItem', {
    portion: DataTypes.INTEGER
  }, {});
  OrderItem.associate = function (models) {
    // associations can be defined here
    OrderItem.belongsTo(models.Order, {
      foreignKey: 'orderId',
      as: 'order'
    });
    OrderItem.belongsTo(models.Dish, {
      foreignKey: 'dishId',
      as: 'dish'
    });
  };
  return OrderItem;
};