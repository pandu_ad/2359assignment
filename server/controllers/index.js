const auth = require('./auth');
const dish = require('./dish');
const order = require('./order');
const admin = require('./user/admin');
const chef = require('./user/chef');
const customer = require('./user/customer');

module.exports = {
    auth,
    dish,
    order,
    admin,
    chef,
    customer
}