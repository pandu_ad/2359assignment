const sequelize = require('sequelize');
const moment = require('moment');

const User = require('../models').User;
const Order = require('../models').Order;
const Dish = require('../models').Dish;
const OrderItem = require('../models').OrderItem;
const userConstants = require('../constants').user;

module.exports = {
    // GET
    // List all orders
    all(req, res) {
        Order.findAll({ include: [{ model: OrderItem, as: "orderItems", include: [{ model: Dish, as: 'dish' }] }] }).then(list => res.status(200).send(list));
    },

    // GET
    // Get estimated waiting time
    waitingTime(req, res) {
        // Find selected dishes
        Dish.findAll({
            where: {
                id: {
                    [sequelize.Op.or]: (req.query.id ? req.query.id : [-1]) // search id === -1 to return an empty array if no query is passed
                }
            },
            attributes: ['duration'],
            order: [['duration', 'ASC']]
        }).then(dishes => {
            if (dishes.length < 1) {
                return res.status(200).send({ message: "Please select at least 1 dish" })
            }

            // Find the other queueing dishes
            OrderItem.findAll({
                where: { userId: null },
                include: [OrderItem.associations.order, OrderItem.associations.dish],
                order: [[OrderItem.associations.order, 'createdAt', 'ASC'], [OrderItem.associations.dish, 'duration', 'ASC']]
            }).then(orderItems => {
                // Find available chefs
                User.findAll({ where: { role: userConstants.role.chef } }).then(chefs => {
                    // Get selected dishes
                    var selectedDishes = dishes.map(dish => { return { duration: dish.duration, doneAt: null } });
                    // Get queueing dishes
                    var queueDishes = orderItems.map(orderItem => { return { duration: orderItem.dish.duration } });
                    // Get chefs
                    var availableChefs = chefs.map(chef => { return { id: chef.id, doneAt: moment(chef.doneAt) } })

                    // Calculate the time each queueing dish served
                    for (var dish of queueDishes) {
                        var assignedChef = availableChefs.find(chef => chef.doneAt.isSame(moment.min(...availableChefs.map(x => x.doneAt)))); // Get chef with earliest free time
                        assignedChef.doneAt = (moment().isSameOrAfter(assignedChef.doneAt) ? moment() : assignedChef.doneAt).add(dish.duration, 'minutes'); // Update chef's free time
                    }

                    // Calculate the time each selected dish served
                    for (var dish of selectedDishes) {
                        var assignedChef = availableChefs.find(chef => chef.doneAt.isSame(moment.min(...availableChefs.map(x => x.doneAt)))); // Get chef with earliest free time
                        assignedChef.doneAt = (moment().isSameOrAfter(assignedChef.doneAt) ? moment() : assignedChef.doneAt).add(dish.duration, 'minutes'); // Update chef's free time
                        dish.doneAt = assignedChef.doneAt; // Update dish's served time
                    }

                    // Total waiting time
                    // Select the latest served selected dish
                    var maxServeTime = moment.max(...selectedDishes.map(x => x.doneAt));
                    var waitingTime = Math.ceil(moment.duration(maxServeTime.diff(moment())).asMinutes()); // Get time diff from now in minutes

                    res.status(201).send({ waitingTime });
                })
            })
        })
    },

    // POST
    // Create new order from customer
    create(req, res) {
        // Check if no item is invalid
        if (!req.body.items) {
            return res.status(400).send({ message: "Order is invalid" })
        }
        // Check if no item is ordered
        if (req.body.items.length === 0) {
            return res.status(200).send({ message: "Order is empty" })
        }
        // Check if any portion is 0
        if (req.body.items.some(x => x.portion < 1)) {
            return res.status(200).send({ message: "Please order at least 1 portion for each item" })
        }

        // Create order
        Order.create(
            {
                orderItems: [...req.body.items]
            },
            {
                include: [Order.associations.orderItems]
            })
            .then(order => {
                // Find queueing dishes (newly ordered dishes included)
                OrderItem.findAll({
                    where: { userId: null },
                    include: [OrderItem.associations.order, OrderItem.associations.dish],
                    order: [[OrderItem.associations.order, 'createdAt', 'ASC'], [OrderItem.associations.dish, 'duration', 'ASC']]
                }).then(orderItems => {
                    // Find available chefs
                    User.findAll({ where: { role: userConstants.role.chef } }).then(chefs => {
                        // Get queueing dishes
                        var queueDishes = orderItems.map(orderItem => {
                            return {
                                id: orderItem.dish.id,
                                orderId: orderItem.orderId,
                                price: orderItem.dish.price,
                                portion: orderItem.portion,
                                duration: orderItem.dish.duration,
                                doneAt: null
                            }
                        });
                        // Get chefs
                        var availableChefs = chefs.map(chef => { return { id: chef.id, doneAt: moment(chef.doneAt) } })

                        // Calculate the time each queueing dish served
                        for (var dish of queueDishes) {
                            var assignedChef = availableChefs.find(chef => chef.doneAt.isSame(moment.min(...availableChefs.map(x => x.doneAt)))); // Get chef with earliest free time
                            assignedChef.doneAt = (moment().isSameOrAfter(assignedChef.doneAt) ? moment() : assignedChef.doneAt).add(dish.duration, 'minutes'); // Update chef's free time
                            dish.doneAt = assignedChef.doneAt; // Update dish's served time
                        }

                        var orderedDishes = queueDishes.filter(dish => dish.orderId === order.id);

                        // Compile order receipt (including the portions)
                        var receipt = orderedDishes.map(dish => { return { id: dish.id, price: dish.price, portion: dish.portion } });
                        // Calculate total price
                        var totalPrice = orderedDishes.reduce((lsum, dish) => lsum + dish.portion * dish.price, 0);
                        // Select the latest served selected dish
                        var maxServeTime = moment.max(...orderedDishes.map(x => x.doneAt));
                        var waitingTime = Math.ceil(moment.duration(maxServeTime.diff(moment())).asMinutes()); // Get time diff from now in minutes

                        res.status(201).send({ receipt, totalPrice, waitingTime });
                    })
                })
            })
            .catch(err => res.status(400).send(err));
    },

    //POST
    // Get dish to be assigned to requesting chef
    assignChef(req, res) {
        // Sort orderItem by the time the order created
        // Then, sort and select an orderItem by the fastest dish duration
        OrderItem.findOne({
            where: { userId: null },
            include: [OrderItem.associations.order, OrderItem.associations.dish],
            order: [[OrderItem.associations.order, 'createdAt', 'ASC'], [OrderItem.associations.dish, 'duration', 'ASC']]
        }).then(orderItem => {
            if (orderItem) {
                // Find chef to be assigned
                User.findOne(
                    {
                        where: {
                            email: req.body.email
                        },
                        attributes: ['id', 'doneAt']
                    })
                    .then(user => {
                        // check if user found
                        if (!user) {
                            res.status(400).send({ message: 'No user found!' });
                        }

                        // Update chef free time
                        var newDoneAt = (moment().isSameOrAfter(moment(user.doneAt)) ? moment() : moment(user.doneAt)).add(orderItem.dish.duration, 'minutes');
                        user.update({ doneAt: moment.utc(newDoneAt).format('YYYY-MM-DD HH:mm:ss') })
                            .then(() => {
                                // assign chef to the orderItem
                                orderItem.update({
                                    userId: user.id
                                })
                                    .then(orderItem => res.status(200).send({ dish: orderItem.dish, portion: orderItem.portion }))
                                    .catch(err => res.status(400).send(err));
                            })
                    })
                    .catch(err => res.status(400).send(err));
            }
            else {
                res.status(200).send({ message: 'No dish left!' })
            }
        })
            .catch(err => res.status(400).send(err));
    },
}