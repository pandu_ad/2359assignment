const Dish = require('../models').Dish;
const fs = require('fs')

module.exports = {
    // GET 
    // List all dishes
    all(req, res) {
        Dish.findAll().then(list => res.status(200).send(list));
    },

    // POST
    // Create new empty dish and pass the id for dish's image name prefix
    create(req, res, next) {
        Dish.create({})
            .then(dish => {
                req.id = dish.id;
                next();
            })
            .catch(err => res.status(400).send(err));
    },

    // PUT
    // Update a dish
    update(req, res) {
        Dish.findOne({
            where: {
                id: (req.body.id ? req.body.id : req.id)
            }
        })
            .then(dish => {
                // Delete previous image
                if (req.files.length > 0 && dish.image) {
                    fs.unlink('public/images/' + dish.image, function (err) {
                        if (err) throw err;
                        console.log('File deleted!');
                    });
                }

                // Update dish
                dish.update({
                    description: req.body.description,
                    image: req.files.length > 0 ? req.files[0].filename : dish.image,
                    price: req.body.price,
                    duration: req.body.duration
                })
                    .then(dish => res.status(200).send(dish))
                    .catch(err => res.status(400).send(err));
            })
            .catch(err => res.status(400).send(err));

    },

    // DELETE
    // Delete a dish
    delete(req, res) {
        // Find dish
        Dish.findOne({
            where: {
                id: req.body.id
            }
        })
            .then(dish => {
                // Remove dish
                dish.destroy()
                    .then(() => res.status(200).send({ message: 'dish deleted!' }))
                    .catch(err => res.status(400).send(err));
            })
            .catch(err => res.status(400).send(err));
    },
}