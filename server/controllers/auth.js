const crypto = require('crypto');
const jwt = require('jsonwebtoken');

const User = require('../models').User;
const jwtConfig = require('../config/jwtConfig');
const userConstants = require('../constants').user;
const customerController = require('./user/customer');

// Check token with given role and provided error message
function tokenCheck(req, res, next, role, errMessage) {
    let token = req.headers['x-access-token'] || req.headers['authorization']; // Express headers are auto converted to lowercase

    // Check if token null/undefined
    if (!token) {
        // Break
        return res.status(401).json({
            success: false,
            message: 'Auth token is not supplied'
        });
    }

    if (token.startsWith('Bearer ')) {
        // Remove Bearer from string
        token = token.slice(7, token.length);
    }

    // Verify token
    jwt.verify(token, jwtConfig.secret, (err, decoded) => {
        if (err) {
            res.status(401).json({
                success: false,
                message: 'Token is not valid'
            });
        } else {
            if (decoded.role === role) {
                req.decoded = decoded;
                next();
            }
            else {
                // Logged in user's role doesn't match the role required
                res.status(401).json({
                    success: false,
                    message: errMessage
                });
            }
        }
    });
}

module.exports = {
    // POST
    // Signup a new customer
    signup(req, res) {
        customerController.create(req, res);
    },

    // POST
    // Login API for all user
    login(req, res) {
        if (!req.body.email || !req.body.password) {
            // Break
            return res.status("400").send("Invalid details!");
        }

        // Find user by email
        User.findOne({
            where: {
                email: req.body.email
            }
        }).then(user => {
            if (user) {
                let password = crypto.pbkdf2Sync(req.body.password, user.salt, 1000, 64, `sha512`).toString(`hex`);
                if (user.password == password) {
                    // Create and send token
                    let token = jwt.sign({
                        id: user.id,
                        role: user.role,
                    }, jwtConfig.secret);
                    res.send({
                        email: user.email,
                        role: user.role,
                        token
                    });
                }
                else {
                    res.status(400).send({ message: "Password wrong" });
                }
            }
            else {
                res.status(400).send({ message: "Account not found" });
            }
        });
    },

    // Middleware to check if admin
    isAdmin(req, res, next) {
        tokenCheck(req, res, next, userConstants.role.admin, "Admin role required");
    },

    // Middleware to check if chef
    isChef(req, res, next) {
        tokenCheck(req, res, next, userConstants.role.chef, "Chef role required");
    },

    // Middleware to check if customer
    isCustomer(req, res, next) {
        tokenCheck(req, res, next, userConstants.role.customer, "Customer role required");
    }
}