const crypto = require('crypto');

const User = require('../../models').User;
const userConstants = require('../../constants').user;

module.exports = {
    // POST
    // create user customer
    create(req, res) {
        // creating a unique salt for a particular user 
        let salt = crypto.randomBytes(16).toString('hex');
        // hashing user's salt and password with 1000 iterations, 64 length and sha512 digest 
        let hash = crypto.pbkdf2Sync(req.body.password, salt, 1000, 64, `sha512`).toString(`hex`);

        // Create user
        User.create({
            email: req.body.email,
            password: hash,
            salt: salt,
            role: userConstants.role.customer
        })
            .then(user => res.status(200).send(user))
            .catch(err => res.status(400).send(err));
    },
}