const crypto = require('crypto');
const Sequelize = require('sequelize');

const User = require('../../models').User;
const userConstants = require('../../constants').user;

module.exports = {
    // GET
    // List all admins
    all(req, res) {
        // Find users except the requesting one (logged in)
        User.findAll({
            where: {
                role: userConstants.role.admin,
                email: {
                    [Sequelize.Op.ne]: req.query.email // email != req.query.email
                }
            }
        }).then(list => res.status(200).send(list.map(user => { return { id: user.id, email: user.email, role: userConstants.roleName[user.role] } })));
    },

    // POST
    // Create a new admin
    create(req, res) {
        // Creating a unique salt for a particular user 
        let salt = crypto.randomBytes(16).toString('hex');
        // Hashing user's salt and password with 1000 iterations, 64 length and sha512 digest 
        let hash = crypto.pbkdf2Sync(req.body.password, salt, 1000, 64, `sha512`).toString(`hex`);

        // Create user
        User.create({
            email: req.body.email,
            password: hash,
            salt: salt,
            role: userConstants.role.admin
        })
            .then(user => res.status(200).send(user))
            .catch(err => res.status(400).send(err));
    },

    // DELETE
    // delete an admin
    delete(req, res) {
        // Find user
        User.findOne({
            where: {
                id: req.body.id
            }
        })
            .then(user => {
                // Remove user
                user.destroy()
                    .then(() => res.status(200).send({ message: 'user admin deleted!' }))
                    .catch(err => res.status(400).send(err));
            })
            .catch(err => res.status(400).send(err));
    },
}