const crypto = require('crypto');

const User = require('../../models').User;
const userConstants = require('../../constants').user;

module.exports = {
    // GET
    // List all chefs
    all(req, res) {
        User.findAll({ where: { role: userConstants.role.chef } }).then(list => res.status(200).send(list.map(user => { return { id: user.id, email: user.email, role: userConstants.roleName[user.role] } })));
    },

    // POST
    // Create a new chef
    create(req, res) {
        // creating a unique salt for a particular user 
        let salt = crypto.randomBytes(16).toString('hex');
        // hashing user's salt and password with 1000 iterations, 64 length and sha512 digest 
        let hash = crypto.pbkdf2Sync(req.body.password, salt, 1000, 64, `sha512`).toString(`hex`);

        // Create user
        User.create({
            email: req.body.email,
            password: hash,
            salt: salt,
            role: userConstants.role.chef
        })
            .then(user => res.status(200).send(user))
            .catch(err => res.status(400).send(err));
    },

    // DELETE
    // Delete a chef
    delete(req, res) {
        // Find user
        User.findOne({
            where: {
                id: req.body.id
            }
        })
            .then(user => {
                // Remove user
                user.destroy()
                    .then(() => res.status(200).send({ message: 'user chef deleted!' }))
                    .catch(err => res.status(400).send(err));
            })
            .catch(err => res.status(400).send(err));
    },
}