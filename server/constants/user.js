module.exports = {
    role: {
        admin: 0,
        chef: 1,
        customer: 2
    },
    roleName: {
        0: "Admin",
        1: "Chef",
        2: "Customer"
    }
}