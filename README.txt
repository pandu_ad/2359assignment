--- Run API server ---
1. Create mysql database named '2359_development'
2. Create mysql account 'admin':'admin123'
3. Open cmd at this directory
4. run "cd server"
5. run "npm install"
6. run "npm install -g sequelize-cli"
7. run "sequelize db:migrate" to create tables
8. run "sequelize db:seed:all" to seed a user roled as admin
9. run "npm start"

--- Run app ---
1. Open cmd at this directory
2. run "cd app"
3. run "npm install"
4. run "npm run build"
5. Open app/dist/index.html on your browser

-- App pages --
/               \\ Login page
/register       \\ Register pages
/home           \\ Admin page
/customer       \\ Customer page
/chef           \\ Chef page