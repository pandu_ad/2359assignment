import Axios from "axios";

Axios.defaults.baseURL = "http://167.205.24.10:8020"

async function post(url, payload, header = null) {
    const options = {
        method: 'POST',
        headers: {
            'content-type': 'application/json',
            ...header
        },
        data: payload,
        url,
    };

    return Axios(options)
        .then(response => {
            return response
        })
}

async function get(url, payload, header = null) {
    const options = {
        method: 'GET',
        headers: {
            'content-type': 'application/json',
            ...header
        },
        params: payload,
        url,
    };

    return Axios(options)
        .then(response => {
            return response
        })
}

async function put(url, payload, header = null) {
    const options = {
        method: 'PUT',
        headers: {
            'content-type': 'application/json',
            ...header
        },
        data: payload,
        url,
    };
    
    return Axios(options)
        .then(response => {
            return response
        })
}

async function destroy(url, payload, header = null) {
    const options = {
        method: 'DELETE',
        headers: {
            'content-type': 'application/json',
            ...header
        },
        data: payload,
        url,
    };

    return Axios(options)
        .then(response => {
            return response
        })
}

export default {
    post,
    get,
    put,
    destroy
}