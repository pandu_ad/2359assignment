import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Login from './views/Login.vue'
import Register from './views/Register.vue'
import CustomerHome from './views/CustomerHome.vue'
import ChefHome from './views/ChefHome.vue'

Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/home',
      name: 'home',
      component: Home,
      meta: {
        requestAuth: true
      }
    },
    {
      path: '/customer',
      name: 'Customer',
      component: CustomerHome,
      meta: {
        requestAuth: true
      }
    },
    {
      path: '/chef',
      name: 'Chef',
      component: ChefHome,
      meta: {
        requestAuth: true
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(x => x.meta.requestAuth)) {
    if (localStorage.getItem("token")) {
      next()
    }
    else {
      next('/login')
    }
  }
  else{
    next()
  }
})

export default router